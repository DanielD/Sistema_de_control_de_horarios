<?php
include('sesion.php');
include('conexion.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Examenes</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/m.js"></script> 
    <script src="js/bootstrap.min.js"></script>
</head>

<body class="bg-body-tertiary">

    <header class="pt-2 text-bg-dark">
        <div class="container">
            <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
                <a href="Home.html" class="d-flex align-items-center my-2 my-lg-0 me-lg-auto text-white text-decoration-none">
                    <svg width="46" height="46" fill="currentColor" class="bi bi-browser-firefox" viewBox="0 0 16 16">
                        <path d="M13.384 3.408c.535.276 1.22 1.152 1.556 1.963a7.98 7.98 0 0 1 .503 3.897l-.009.077a8.533 8.533 0 0 1-.026.224A7.758 7.758 0 0 1 .006 8.257v-.04c.016-.363.055-.724.114-1.082.01-.074.075-.42.09-.489l.01-.051a6.551 6.551 0 0 1 1.041-2.35c.217-.31.46-.6.725-.87.233-.238.487-.456.758-.65a1.5 1.5 0 0 1 .26-.137c-.018.268-.04 1.553.268 1.943h.003a5.744 5.744 0 0 1 1.868-1.443 3.597 3.597 0 0 0 .021 1.896c.07.047.137.098.2.152.107.09.226.207.454.433l.068.066.009.009a1.933 1.933 0 0 0 .213.18c.383.287.943.563 1.306.741.201.1.342.168.359.193l.004.008c-.012.193-.695.858-.933.858-2.206 0-2.564 1.335-2.564 1.335.087.997.714 1.839 1.517 2.357a3.72 3.72 0 0 0 .439.241c.076.034.152.065.228.094.325.115.665.18 1.01.194 3.043.143 4.155-2.804 3.129-4.745v-.001a3.005 3.005 0 0 0-.731-.9 2.945 2.945 0 0 0-.571-.37l-.003-.002a2.679 2.679 0 0 1 1.87.454 3.915 3.915 0 0 0-3.396-1.983c-.078 0-.153.005-.23.01l-.042.003V4.31h-.002a3.882 3.882 0 0 0-.8.14 6.454 6.454 0 0 0-.333-.314 2.321 2.321 0 0 0-.2-.152 3.594 3.594 0 0 1-.088-.383 4.88 4.88 0 0 1 1.352-.289l.05-.003c.052-.004.125-.01.205-.012C7.996 2.212 8.733.843 10.17.002l-.003.005.003-.001.002-.002h.002l.002-.002a.028.028 0 0 1 .015 0 .02.02 0 0 1 .012.007 2.408 2.408 0 0 0 .206.48c.06.103.122.2.183.297.49.774 1.023 1.379 1.543 1.968.771.874 1.512 1.715 2.036 3.02l-.001-.013a8.06 8.06 0 0 0-.786-2.353Z"/>
                    </svg>
                    <h4 style="margin-left: 15px; margin-top: 15px;"><?php echo $tipo; ?></h4>
                </a>
                <ul class="nav col-12 col-lg-auto my-2 justify-content-center my-md-0 text-small">
                    <li>
                        <a href="Home.html" class="nav-link text-white btn btn-dark">
                            <svg class="bi d-block mx-auto mb-1" 
                                width="24" height="24" fill="currentColor" viewBox="0 0 16 16">
                                <path d="M8.707 1.5a1 1 0 0 0-1.414 0L.646 8.146a.5.5 0 0 0 .708.708L2 8.207V13.5A1.5 1.5 0 0 0 3.5 15h9a1.5 1.5 0 0 0 1.5-1.5V8.207l.646.647a.5.5 0 0 0 .708-.708L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.707 1.5ZM13 7.207V13.5a.5.5 0 0 1-.5.5h-9a.5.5 0 0 1-.5-.5V7.207l5-5 5 5Z"/>
                            </svg>
                            Home
                        </a>
                    </li>
                    <li>
                        <a href="horarios.php" class="nav-link text-white btn btn-dark">
                            <svg class="bi d-block mx-auto mb-1" 
                                width="24" height="24" fill="currentColor" viewBox="0 0 16 16">
                                <path fill-rule="evenodd" d="M0 .5A.5.5 0 0 1 .5 0h4a.5.5 0 0 1 0 1h-4A.5.5 0 0 1 0 .5Zm0 2A.5.5 0 0 1 .5 2h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5Zm9 0a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5Zm-9 2A.5.5 0 0 1 .5 4h3a.5.5 0 0 1 0 1h-3a.5.5 0 0 1-.5-.5Zm5 0a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5Zm7 0a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 0 1h-3a.5.5 0 0 1-.5-.5Zm-12 2A.5.5 0 0 1 .5 6h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5Zm8 0a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5Zm-8 2A.5.5 0 0 1 .5 8h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5Zm7 0a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5Zm-7 2a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 0 1h-8a.5.5 0 0 1-.5-.5Zm0 2a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5Zm0 2a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5Z"/>
                            </svg>
                            Horarios
                        </a>
                    </li>
                    <li>
                        <a href="examenes.php" class="nav-link text-secondary btn btn-dark">
                            <svg class="bi d-block mx-auto mb-1"
                                width="24" height="24" fill="currentColor" viewBox="0 0 16 16">
                                <path d="M9.5 0a.5.5 0 0 1 .5.5.5.5 0 0 0 .5.5.5.5 0 0 1 .5.5V2a.5.5 0 0 1-.5.5h-5A.5.5 0 0 1 5 2v-.5a.5.5 0 0 1 .5-.5.5.5 0 0 0 .5-.5.5.5 0 0 1 .5-.5h3Z"/>
                                <path d="M3 2.5a.5.5 0 0 1 .5-.5H4a.5.5 0 0 0 0-1h-.5A1.5 1.5 0 0 0 2 2.5v12A1.5 1.5 0 0 0 3.5 16h9a1.5 1.5 0 0 0 1.5-1.5v-12A1.5 1.5 0 0 0 12.5 1H12a.5.5 0 0 0 0 1h.5a.5.5 0 0 1 .5.5v12a.5.5 0 0 1-.5.5h-9a.5.5 0 0 1-.5-.5v-12Z"/>
                                <path d="M10.854 7.854a.5.5 0 0 0-.708-.708L7.5 9.793 6.354 8.646a.5.5 0 1 0-.708.708l1.5 1.5a.5.5 0 0 0 .708 0l3-3Z"/>
                            </svg>
                            Examenes
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </header>

    <?php if ($tipo=='administrador' or $tipo=='docente') { 
    $sql = "SELECT * FROM aula";
    $resultado = $con->query($sql);
    $s1 = "SELECT * FROM examen";
    $r1 = $con->query($s1);
    $r1 = $r1->fetch_assoc();
    $fecha = $r1['fecha']
    ?>
    <main class="container pt-5">
		<div class="row g-5">
			<div class="table-responsive small">
				<h2>Examenes</h2>
                <div class="col-md-2">
                    <input type="date" class="form-control" id="fecha" value="<?php echo $fecha; ?>">
                </div>
				<table class="table table-striped mt-3">
					<thead>
						<tr>
							<th scope="col" class="text-center">Aula</th>
							<th scope="0" class="text-center">07:00-09:00</th>
							<th scope="1" class="text-center">09:00-11:00</th>
							<th scope="1" class="text-center">11:00-13-00</th>
                            <th scope="1" class="text-center">14:00-16:00</th>
                            <th scope="1" class="text-center">16:00-18:00</th>
                            <th scope="1" class="text-center">18:00-20:00</th>
                            <th scope="1" class="text-center">20:00-22:00</th>
						</tr>
					</thead>
					<tbody id='h'>
                        <?php while ($row = $resultado->fetch_assoc()) {?>
					    	<tr>
                                <td class="text-center"><?php echo $row['nombre'] ?></td>
                                <?php
                                    $idaula = $row['id'];
                                    $sql2 = "SELECT * FROM examen e
                                    LEFT JOIN materias m ON e.idm=m.id
                                    WHERE fecha='$fecha' and e.ida=$idaula";
                                    $resultado2 = $con->query($sql2);
                                    $info = [];
                                    while ($r = $resultado2->fetch_assoc()){
                                        if ($r['hora_inicio']=='07:00:00' & $r['hora_fin']=='09:00:00'){
                                            $info[1] = [$r['materia']];
                                        }else if (($r['hora_inicio']=='09:00:00' & $r['hora_fin']=='11:00:00')){
                                            $info[2] = [$r['materia']];
                                        }else if (($r['hora_inicio']=='11:00:00' & $r['hora_fin']=='13:00:00')){
                                            $info[3] = [$r['materia']];
                                        }else if (($r['hora_inicio']=='14:00:00' & $r['hora_fin']=='16:00:00')){
                                            $info[4] = [$r['materia']];
                                        }else if (($r['hora_inicio']=='16:00:00' & $r['hora_fin']=='18:00:00')){
                                            $info[5] = [$r['materia']];
                                        }else if (($r['hora_inicio']=='18:00:00' & $r['hora_fin']=='20:00:00')){
                                            $info[6] = [$r['materia']];
                                        }else if (($r['hora_inicio']=='20:00:00' & $r['hora_fin']=='22:00:00')){
                                            $info[7] = [$r['materia']];
                                        }
                                    }
                                    for ($i=1; $i<=7; $i++) {
                                        if (isset($info[$i])){
                                            $materia = $info[$i][0];
                                            echo '<td class="text-center">'.$materia.'</td>';
                                        }else{
                                            echo '<td class="text-center">-</td>';
                                        }
                                    }
                                ?>
					    	</tr>
                        <?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</main>

    <?php } else {
        $sql2 = "SELECT * FROM examen e
        LEFT JOIN materias m ON e.idm=m.id
        LEFT JOIN aula a ON e.ida=a.id
        LEFT JOIN inscripciones i ON e.idm=i.idm
        WHERE i.ide=$id";
        $resultado2 = $con->query($sql2);
    ?>
    <main class="container pt-5">
		<div class="row g-5">
			<div class="table-responsive small">
				<h2>Examenes</h2>
				<table class="table table-striped table-sm mt-3">
					<thead>
						<tr>
                            <th scope="col" class="text-center">Fecha</th>
							<th scope="0" class="text-center">07:00-09:00</th>
							<th scope="1" class="text-center">09:00-11:00</th>
							<th scope="1" class="text-center">11:00-13-00</th>
                            <th scope="1" class="text-center">14:00-16:00</th>
                            <th scope="1" class="text-center">16:00-18:00</th>
                            <th scope="1" class="text-center">18:00-20:00</th>
                            <th scope="1" class="text-center">20:00-22:00</th>
						</tr>
					</thead>
					<tbody id='h'>
                        <?php while ($row = $resultado2->fetch_assoc()) {?>
					    	<tr>
                                <td class="text-center"><?php echo $row['fecha'] ?></td>
                                <?php
                                    $fecha = $row['fecha'];
                                    $sql = "SELECT * FROM examen e
                                    LEFT JOIN materias m ON e.idm=m.id
                                    LEFT JOIN aula a ON e.ida=a.id
                                    LEFT JOIN inscripciones i ON e.idm=i.idm
                                    WHERE i.ide=$id and e.fecha='$fecha'";
                                    $resultado = $con->query($sql);
                                    $info = [];
                                    while ($r = $resultado->fetch_assoc()){
                                        if ($r['hora_inicio']=='07:00:00' & $r['hora_fin']=='09:00:00'){
                                            $info[1] = [$r['materia'],$r['nombre'],$r['ida']];
                                        }else if (($r['hora_inicio']=='09:00:00' & $r['hora_fin']=='11:00:00')){
                                            $info[2] = [$r['materia'],$r['nombre'],$r['ida']];
                                        }else if (($r['hora_inicio']=='11:00:00' & $r['hora_fin']=='13:00:00')){
                                            $info[3] = [$r['materia'],$r['nombre'],$r['ida']];
                                        }else if (($r['hora_inicio']=='14:00:00' & $r['hora_fin']=='16:00:00')){
                                            $info[4] = [$r['materia'],$r['nombre'],$r['ida']];
                                        }else if (($r['hora_inicio']=='16:00:00' & $r['hora_fin']=='18:00:00')){
                                            $info[5] = [$r['materia'],$r['nombre'],$r['ida']];
                                        }else if (($r['hora_inicio']=='18:00:00' & $r['hora_fin']=='20:00:00')){
                                            $info[6] = [$r['materia'],$r['nombre'],$r['ida']];
                                        }else if (($r['hora_inicio']=='20:00:00' & $r['hora_fin']=='22:00:00')){
                                            $info[7] = [$r['materia'],$r['nombre'],$r['ida']];
                                        }
                                    }
                                    for ($i=1; $i<=7; $i++) {
                                        if (isset($info[$i])){
                                            $materia = $info[$i][0];
                                            $naula = $info[$i][1];
                                            $idaula = $info[$i][2];
                                            echo '<td class="text-center">'.$materia.' - '.$naula.'</td>';
                                        }else{
                                            echo '<td class="text-center">-</td>';
                                        }
                                    }
                                ?>
					    	</tr>
                        <?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</main>
    <?php } ?>


    <script>
        document.getElementById('fecha').addEventListener('change', function (event){
            var fecha = event.target.value;
            var contenedor = document.getElementById('h');
            var ajax = new XMLHttpRequest()
            ajax.open("get", 'actulizarEx.php?fecha='+fecha , true);
            ajax.onreadystatechange = function () {
                if (ajax.readyState == 4) {
                    contenedor.innerHTML = ajax.responseText;
                }
            }
            ajax.setRequestHeader("Content-Type", "text/html; charset=utf-8");
            ajax.send();
        })
    </script>

</body>

<footer class="text-center text-white py-3 bg-dark" style="position: absolute; bottom: 0; width: 100%;">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h5>Contacto</h5>
                <p>Teléfono: (123) 456-7890</p>
                <p>Correo electrónico: info@example.com</p>
                <!-- Otra información de contacto -->
            </div>
            <div class="col-md-4">
                <h5>Soporte</h5>
                
                <p><a href="">contacto soporte</a></p>
    
                <!-- Otros enlaces útiles -->
            </div>
            <div class="col-md-4">
                <h5>Redes Sociales</h5>
                <p><a href="https://www.facebook.com/tupagina">Facebook</a></p>
                <p><a href="https://twitter.com/tupagina">Twitter</a></p>
                <!-- Otros enlaces a redes sociales -->
            </div>
        </div>
    </div>
</footer>
</html>